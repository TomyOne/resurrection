#!/bin/sh

mount /run/archiso/cowspace -o remount,size=2G || { echo "Failed to increase 'cowspace'"; exit 1; }

# Check for UEFI platform
efivar --list > /dev/null || { echo "Non UEFI platform"; exit 2; }

# Install Rustup multiplexer through pacman
sudo pacman --noconfirm -S rustup || { echo "Failed to install rustup"; exit 3; }

# Install Rust x86_64 toolchain
rustup toolchain install stable-x86_64-unknown-linux-gnu || { echo "Failed to 
download rust toolchain"; exit 4; }

# Setup x86_64 Rust toolchain as default
rustup default stable-unknown-linux-gnu || { echo "Failed to setup rust toolchain 
as default"; exit 5; }

# Set root password
echo "Set root password:"
while true; do
    passwd
    if [ $? -eq 0 ]; then
        break
    else; then
        echo "Enter correct password"
    fi
done

# Start sshd
systemctl start sshd.service || { echo "Failed to start sshd"; exit 6; }

# Display IP address
ip a s || { echo "Failed to list network interfaces"; exit 7; }
