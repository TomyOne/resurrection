![Toaster shopping](cylon.png)

# Resurrection

Once a while you need to install the Linux on your machine. To ease that task it 
would be great to automate it. 

This script installs fully encrypted Archlinux on UEFI machine.



## Download and verify

Download the `archlinux-<version>-x86_64.iso` and verify by running
```bash
gpg --keyserver pgp.mit.edu --keyserver-options auto-key-retrieve --verify archlinux-<version>-x86_64.iso.sig
```

* Add public keys into keyring
* Verify the files
* Write the files to the USB stick

## Enable SSH

In the Virtual machine or on the PC change root password:

```bash
passwd
```

Check that option `PermitRootLogin yes` is present in `/etc/ssh/sshd_config`.

Start the sshd service

```bash
systemctl start sshd.service
```

Get the IP address of the machine:

```bash
ip a s
```

## Connect over SSH

On the remote machine run SSH client:

```bash
ssh root@<IP_ADDR>
```

## Bootstrapping
The script itself is written in Rust therefore you'll need to install it first by running the `bootstrap.sh` script which downloads and setup *Rust* on the RAM disk of the *Archlinux*.

## QEMU setup
Ensure QEMU and OMVF (open source UEFI) packages are installed. Then just run `qemu.sh` script which:
* Verifies the ISO using GPG
* Copies UEFI variables
* Creates QEMU disk
* Run QEMU with OMVF firmware
* Through QEMU monitor starts the SSH deamon in guest and sets root password
* Over SSH installs git and downloads this repository
* Launches the `00_arch_install.sh` script

```bash
# Single drive system (SSD | HDD)
./qemu.sh <ARCH_ISO>

# Dual drive system (SSD & HDD)
DUAL="" ./qemu.sh <ARCH_ISO>
```



## Dependencies
The main dependency is of course Archlinux distribution and Rust toolchain.
Beside these there are several useful crates.
* clap
* duct
* log
* failure

## TODO
* Switch QEMU into console mode to interact better with the guest shell
* Integrate `remaster.sh` into workflow
* Automate installation in could (GITLAB CI)
* Finish installation of the userspace (GUI, audio, android ...)
* Rewrite in **Rust**
* Config file for packages to install
* As first step inquire user about the passwords so that he doesn't have to type them several times (create and decrypt partition, root and user password change)
* Test Systemd-boot as bootloader
* Add secure boot
* Put TODOs into issues
* Running in QEMU now ends in endless loop when asked for input

## Quick boot code

```bash
  pacman -Sy git && git clone https://gitlab.com/phodina/resurrection && cd resurrection/scripts
  # Single drive system
  UNAME="user" UNAME_PASS="123" ROOT_PASS="abc" HNAME="queen" SSD=/dev/sda ./00_arch_install.sh
  
  # Dual drive system
  UNAME="user" UNAME_PASS="123" ROOT_PASS="abc" HNAME="queen" SSD=/dev/sda DUAL="" HDD=/dev/sdb ./00_arch_install.sh
```
