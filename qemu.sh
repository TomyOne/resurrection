#!/bin/sh
if [ "$#" -eq 1 ]
then
    INSTALL_ISO=$1
else
	echo "Arguments are not equals to: qemu.sh <INSTALL_ISO>"
    exit 1
fi


if [ -n "$DUAL" ]; then echo "Requesting single disk install ... "; else echo "Requesting dual disk install ... "; fi

if [ -f "$INSTALL_ISO" ]; then echo "Archlinux image set to $INSTALL_ISO"; else echo "Please pass the Archlinux image for installation" && exit 1; fi


QEMU_TARGET_DIR=qemu 

QEMU_DISK=$QEMU_TARGET_DIR/arch_disk
QEMU_DISK_SIZE=12G


QEMU_DISK_2=$QEMU_TARGET_DIR/arch_disk_2
QEMU_DISK_SIZE_2=12G

QEMU_MONITOR_PORT=10101


UEFI_VARS=qemu/efi_vars.bin
UEFI_VARS_DEFAULT=/usr/share/ovmf/x64/OVMF_VARS.fd
UEFI_CODE=/usr/share/ovmf/x64/OVMF_CODE.fd

# Verify image

if ! gpg --keyserver pgp.mit.edu --keyserver-options auto-key-retrieve --verify "$INSTALL_ISO".sig;
then
    echo "Failed to verify installation ISO"; exit 1
fi

    

# Create `qemu` directory if necessary 
if [ ! -d "$QEMU_TARGET_DIR" ]
then
    echo "Creating target dir ..."
    mkdir $QEMU_TARGET_DIR
fi

# Check QEMU disk image exists 
if [ ! -f "$QEMU_DISK" ]
then
    echo "Creating QEMU disk image ..."
    # Create disk
    qemu-img create -f qcow2 $QEMU_DISK $QEMU_DISK_SIZE || { echo "Failed to crate QEMU disk image"; exit 2; }
    
    # Disable CopyOnWrite
    chattr +C $QEMU_DISK
fi

if [ -z "$DUAL" ]
then
    if [ ! -f "$QEMU_DISK_2" ]
    then
        echo "Creating second QEMU disk image ..."
        # Create disk
        qemu-img create -f qcow2 $QEMU_DISK_2 $QEMU_DISK_SIZE_2 || { echo "Failed to crate second QEMU disk image"; exit 2; }
        
        # Disable CopyOnWrite
        chattr +C $QEMU_DISK_2
    fi
fi

# Check copy of UEFI variables exists
if [ ! -f "$UEFI_VARS" ]
then
    # Check default UEFI variables provided by OVMF exist 
    if [ -f "$UEFI_VARS_DEFAULT" ]
    then
        cp $UEFI_VARS_DEFAULT $UEFI_VARS
    else
        echo "UEFI variables don't exist!"; exit 1
    fi
fi


# Install system
if [ -n "$DUAL" ]
then
    echo "Running Arch image [single]..."
    qemu-system-x86_64  -cdrom "$INSTALL_ISO" \
                        -boot order=d -hda "$QEMU_DISK" \
                        -m 4096M -enable-kvm -net nic,model=virtio \
                        -net user,hostfwd=tcp::2222-:22 \
                        -monitor tcp:127.0.0.1:"$QEMU_MONITOR_PORT",server,nowait \
                        -drive if=pflash,format=raw,readonly,file="$UEFI_CODE" \
                        -drive if=pflash,format=raw,file="$UEFI_VARS" || { echo "Failed to start QEMU"; exit 3; } &
else
    echo "Running Arch image [dual]..."
    qemu-system-x86_64  -cdrom "$INSTALL_ISO" \
                        -boot order=d -hda "$QEMU_DISK" -hdb "$QEMU_DISK_2" \
                        -m 4096M -enable-kvm -net nic,model=virtio \
                        -net user,hostfwd=tcp::2222-:22 \
                        -monitor tcp:127.0.0.1:"$QEMU_MONITOR_PORT",server,nowait \
                        -drive if=pflash,format=raw,readonly,file="$UEFI_CODE" \
                        -drive if=pflash,format=raw,file="$UEFI_VARS" || { echo "Failed to start QEMU"; exit 3; } &
fi

#TODO: Create a loop and periodically write to device attached to QEMU behaving as serial port in guest and check for output in host
echo "Wait 50s unitl Archlinux boots to root tty ..."
sleep 50

#TODO: Ask user for password
#TODO: Generate keys files in tmp
# Connect to monitor - start SSH deamon
echo "sendkey s-y-s-t-e-m-c-t-l-spc-s-t-a-r-t-spc-s-s-h-d-kp_enter" | nc 127.0.0.1 "$QEMU_MONITOR_PORT" -q 3
echo "sendkey p-a-s" | nc 127.0.0.1 "$QEMU_MONITOR_PORT" -q 3
echo "sendkey s-w-d-kp_enter" | nc 127.0.0.1 "$QEMU_MONITOR_PORT" -q 3
echo "sendkey a-b-c-kp_enter" | nc 127.0.0.1 "$QEMU_MONITOR_PORT" -q 3
echo "sendkey a-b-c-kp_enter" | nc 127.0.0.1 "$QEMU_MONITOR_PORT" -q 3


# Clean known hosts
sed -i 's/^\[localhost\]:2222.*//' ~/.ssh/known_hosts

#TODO: Write password to tmp file
# Log over SSH
if [ -n "$DUAL" ]
then
echo "Connect over SSH [single]..."
sshpass -p 'abc' ssh -o StrictHostKeyChecking=no root@localhost -p 2222 << EOF
# Increase space
mount /run/archiso/cowspace -o remount,size=1G || {echo "Failed to increase 'cowspace'"; exit 1; }
# Update system and install git
pacman --noconfirm -Suy git || { echo "Failed to install git"; exit 2; }
git clone https://gitlab.com/phodina/resurrection.git || { echo "Failed to clone repo"; exit 3; }
cd resurrection/scripts
QEMU="" UNAME="user" UNAME_PASS="123" ROOT_PASS="abc" HNAME="queen" SSD=/dev/sda ./00_arch_install.sh
EOF
else
echo "Connect over SSH [dual]..."
sshpass -p 'abc' ssh -o StrictHostKeyChecking=no root@localhost -p 2222 << EOF
# Increase space
mount /run/archiso/cowspace -o remount,size=1G || {echo "Failed to increase 'cowspace'"; exit 1; }
# Update system and install git
pacman --noconfirm -Suy git || { echo "Failed to install git"; exit 2; }
git clone https://gitlab.com/phodina/resurrection.git || { echo "Failed to clone repo"; exit 3; }
cd resurrection/scripts
# Dual drive system
QEMU="" UNAME="user" UNAME_PASS="123" ROOT_PASS="abc" HNAME="queen" SSD=/dev/sda HDD=/dev/sdb ./00_arch_install.sh
EOF
fi

# Start installed system
if [ -n "$DUAL" ]
then
    echo "Running Arch image [single]..."
    qemu-system-x86_64  -boot order=d -hda "$QEMU_DISK" \
                        -m 4096M -enable-kvm -net nic,model=virtio \
                        -net user,hostfwd=tcp::2222-:22 \
                        -drive if=pflash,format=raw,readonly,file="$UEFI_CODE" \
                        -drive if=pflash,format=raw,file="$UEFI_VARS" || { echo "Failed to start QEMU"; exit 3; } &
else
    echo "Running Arch image [dual]..."
    qemu-system-x86_64  -boot order=d -hda "$QEMU_DISK" -hdb "$QEMU_DISK_2" \
                        -m 4096M -enable-kvm -net nic,model=virtio \
                        -net user,hostfwd=tcp::2222-:22 \
                        -drive if=pflash,format=raw,readonly,file="$UEFI_CODE" \
                        -drive if=pflash,format=raw,file="$UEFI_VARS" || { echo "Failed to start QEMU"; exit 3; } &
fi
