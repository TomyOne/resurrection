use failure::Error;

pub fn bootstrap() -> Result<(),Error> {
/*
# Install base packages to /mnt
pacstrap /mnt base
*/
    Ok(())
}

fn keyfiles() -> Result<(),Error> {
/*
## Add keyfile
cryptsetup luksAddKey /dev/disk/by-partlabel/crypthome /home.key

## Add keyfile
cryptsetup luksAddKey /dev/disk/by-partlabel/cryptswap /swap.key

## Copy keyfiles
cp /home.key /swap.key /mnt/root
*/
    Ok(())
}

fn cryptsetup() -> Result<(),Error> {
/*
UUID_CRYPTHOME=$(blkid -s UUID -o value /dev/disk/by-partlabel/crypthome)
UUID_CRYPTSWAP=$(blkid -s UUID -o value /dev/disk/by-partlabel/cryptswap)

cat >> /mnt/etc/crypttab <<EOL
home    UUID=$UUID_CRYPTHOME    /root/home.key
swap    UUID=$UUID_CRYPTSWAP    /root/swap.key
EOL
*/
Ok(())
}

fn fstab() -> Result<(),Error> {
/*
# Generate filesystem table
genfstab -L -p /mnt >> /mnt/etc/fstab

sed -i 's#LABEL="home"#/dev/mapper/home#g' /mnt/etc/fstab
sed -i 's#LABEL="swap"#/dev/mapper/swap#g' /mnt/etc/fstab
*/
    Ok(())
}
