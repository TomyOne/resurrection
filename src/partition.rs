use failure::Error;

pub fn partition () -> Result<(),Error> {

    Ok(())
}

fn clear_partitions() -> Result<(),Error> {
/*
sgdisk --zap-all $SSD
sgdisk --zap-all $HDD
*/
    Ok(())
}

fn create_partitions() -> Result<(), Error> {
/*
# Create three partitions: EFI, encrypted swap and system
sgdisk --clear \
       --new=1:0:+256MiB --typecode=1:ef00 --change-name=1:EFI \
       --new=2:0:+512MiB --typecode=2:8200 --change-name=2:boot \
       --new=3:0:0       --typecode=3:8200 --change-name=3:cryptsystem \
         $SSD

sgdisk --clear \
       --new=1:0:+8GiB   --typecode=1:8200 --change-name=1:cryptswap \
       --new=2:0:0       --typecode=2:8200 --change-name=2:crypthome \
         $HDD
         */
         Ok(())
}

fn check_partitions() -> Result<(),Error> {
/*
"/dev/disk/by-partlabel/EFI"
"/dev/disk/by-partlabel/cryptswap"
"/dev/disk/by-partlabel/cryptsystem"
"/dev/disk/by-partlabel/crypthome"
*/
    Ok(())
}

fn create_efi_partition() -> Result<(), Error> {
/*
# Create FAT32 filesystem for EFI partition
mkfs.fat -F32 -n EFI /dev/disk/by-partlabel/EFI
*/
    Ok(())
}

fn create_encrypted_system_partition () -> Result<(), Error> {
/*
# Encrypt system partition
cryptsetup luksFormat --align-payload=8192 -s 256 -c aes-xts-plain64 /dev/disk/by-partlabel/cryptsystem

# Open system partition
cryptsetup open /dev/disk/by-partlabel/cryptsystem system

# Create Btrfs filesystem on system
mkfs.btrfs --force --label system /dev/mapper/system
*/
    Ok(())
}

/// Create encrypted home partition
fn create_encrypted_home_partition () -> Result<(), Error> {
/*
# Encrypt home partition
cryptsetup luksFormat --align-payload=8192 -s 256 -c aes-xts-plain64 /dev/disk/by-partlabel/crypthome

## Create keyfile of 2048 random bytes
dd bs=512 count=4 if=/dev/urandom of=/home.key iflag=fullblock

# Open system partition
cryptsetup open /dev/disk/by-partlabel/crypthome home

# Create Btrfs filesystem on home
mkfs.btrfs --force --label home /dev/mapper/home
*/
    Ok(())
}

/// Create encrypted swap partition
fn create_encrypted_swap_partition () -> Result<(), Error> {
/*
## Swap
# Encrypt swap partition
cryptsetup luksFormat /dev/disk/by-partlabel/cryptswap

## Create keyfile of 2048 random bytes
dd bs=512 count=4 if=/dev/urandom of=/swap.key iflag=fullblock

#Open swap partition
cryptsetup open /dev/disk/by-partlabel/cryptswap swap

# Create swap partition
mkswap -L swap /dev/mapper/swap

# Mount swap
swapon -L swap
*/
    Ok(())
}
