extern crate clap;
#[macro_use]
extern crate duct;
extern crate failure;
extern crate log;

mod partition;
mod mount;
mod bootstrap;

use std::process;
use failure::Error;
use clap::{Arg, App};

// TODO: try exit 1 to check for return value

// Module base_install
// Module chroot -> ...
// Module finito

fn run() -> Result<(), Error> {

    let _partitions = partition::partition()?;
    let _mount = mount::mount()?;
    let _bootstrap = bootstrap::bootstrap()?;
    
    /*
    # chroot into the new system
    arch-chroot /mnt
    */
    
    let current_branch = cmd!("ls", "/dev/stargate").read()?;
    Ok(())
}

fn main() {

    let matches = App::new("Resurrection")
                          .version("0.1.0")
                          .author("Petr H. <hodinapetr46@gmail.com>")
                          .about("Installs Archlinux on your machine")
                          .arg(Arg::with_name("config")
                               .short("c")
                               .long("config")
                               .value_name("FILE")
                               .help("Sets a custom config file")
                               .takes_value(true))
                          .get_matches();
           
    // TODO: Differentiate between root and chroot env
    if let Err(e) = run() {
        println!("Application error: {}", e);

        process::exit(1);
    }
}
