use failure::Error;

pub fn mount() -> Result<(), Error> {
 Ok(())
}


fn mount_system() -> Result<(), Error> {
/*
# Temporarily mount system partition
mount -t btrfs LABEL=system /mnt

# Create the main subvolume 'ROOT'
btrfs subvolume create /mnt/ROOT

# Create subvolume 'tmp'
btrfs subvolume create /mnt/tmp

# Create subvolume 'var'
btrfs subvolume create /mnt/var

# Create subvolume 'snapshots'
btrfs subvolume create /mnt/snapshots

# Create var $o with default options
o=defaults,x-mount.mkdir

# Create var $o_btrfs_sdd with default options for btrfs on SSD
o_btrfs_ssd=$o,compress=lzo,ssd,noatime

# Create var $o_btrfs_sdd with default options for btrfs on SSD
o_btrfs_hdd=$o,compress=lzo

# Mount '.active' subvolume
mount -t btrfs -o subvol=ROOT,$o_btrfs_ssd LABEL=system /mnt/

# Mount 'var' subvolume
mount -t btrfs -o subvol=var,$o_btrfs_ssd LABEL=system /mnt/var

# Mount 'var' subvolume
mount -t btrfs -o subvol=tmp,$o_btrfs_ssd LABEL=system /mnt/tmp
*/
 Ok(())
}


fn mount_home() -> Result<(), Error> {
/*
# Create home mount point
mkdir -p /mnt/home

# Temporarily mount home partition
mount -t btrfs LABEL=home /mnt/home

# Create the main subvolume 'HOME'
btrfs subvolume create /mnt/home/HOME

# Create subvolume 'snapshots'
btrfs subvolume create /mnt/home/snapshots

# Unmount btrfs partition
umount -R /mnt

# Mount 'home' subvolume
mount -t btrfs -o subvol=HOME,$o_btrfs_hdd LABEL=home /mnt/home
*/
 Ok(())
}

/// Mount EFI partition
fn mount_boot() -> Result<(), Error> {
/*
# Create boot efi point
mkdir -p /mnt/boot/efi

# Mount EFI partition
mount LABEL=EFI /mnt/boot/efi
*/
 Ok(())
}
