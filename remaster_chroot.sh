#!/bin/sh

pacman-key --init || (echo "Failed to init pacman keyring" && exit 1)
pacman-key --populate archlinux || (echo "Failed to populate pacman keyring" && exit 2)

# Install Rustup multiplexer through pacman
sudo pacman --noconfirm -S rustup git gcc || (echo "Failed to install rustup" && exit 3)

# Download the repo
git clone https://gitlab.com/phodina/resurrection.git

# Install Rust x86_64 toolchain
rustup toolchain install stable-x86_64-unknown-linux-gnu || (echo "Failed to download rust toolchain" && exit 4)

# Setup x86_64 Rust toolchain as default
rustup default stable-unknown-linux-gnu || (echo "Failed to setup rust toolchain as default" && exit 5)

LANG=C pacman -Sl | awk '/\[installed\]$/ {print $1 "/" $2 "-" $3}' > /pkglist.txt || (echo "Failed to generate package list" && exit 6)

# Clear cache to reduce size of the ISO 
pacman -Scc || (echo "Failed to clear cache" && exit 7)

# Exit the chroot
exit 0
