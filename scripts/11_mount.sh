#!/bin/bash

##################################################
## Mount system partition and create subvolumes ##
##################################################

printf "%s" "Mount system partition and create subvolumes ... "

# Temporarily mount system partition
mount -t btrfs LABEL=system /mnt || { echo "Failed to mount 'system' partition"; exit 1; }

# Create the main subvolume 'ROOT'
btrfs subvolume create /mnt/ROOT || { echo "Failed to create 'ROOT' subvolume"; exit 2; }

# Create subvolume 'tmp'
btrfs subvolume create /mnt/tmp || { echo "Failed to create 'tmp' subvolume"; exit 2; }

# Create subvolume 'var'
btrfs subvolume create /mnt/var || { echo "Failed to create 'var' subvolume"; exit 2; }

if [ -z "$DUAL" ]
then
	# Create subvolume 'home'
	btrfs subvolume create /mnt/home || { echo "Failed to create 'home' subvolume"; exit 2; }
fi

# Create subvolume 'snapshots'
btrfs subvolume create /mnt/snapshots || { echo "Failed to create 'snapshots' subvolume"; exit 2; }

echo "Done"

################################################
## Mount home partition and create subvolumes ##
################################################

if [ -z "$DUAL" ]
then
	printf "%s" "Mount home partition and create subvolumes ... "

	# Create home mount point
	mkdir -p /mnt/home || { echo "Failed to create mount point for 'home' partition"; exit 3; }

	# Temporarily mount home partition
	mount -t btrfs LABEL=home /mnt/home || { echo "Failed to mount 'home' partition"; exit 4; }

	# Create the main subvolume 'HOME'
	btrfs subvolume create /mnt/home/HOME || { echo "Failed to create 'HOME' subvolume"; exit 5; }

	# Create subvolume 'snapshots'
	btrfs subvolume create /mnt/home/snapshots || { echo "Failed to create 'snapshots' subvolume"; exit 5; }

	echo "Done"
fi

#######################################
## Mount system partition subvolumes ##
#######################################

printf "%s" "Mount system subvolumes ... "

# Unmount btrfs partition
umount -R /mnt || { echo "Failed to unmount 'system' partition"; exit 6; }

# Create var $o with default options
o=defaults,x-mount.mkdir

# Create var $o_btrfs_sdd with default options for btrfs on SSD
o_btrfs_ssd=$o,compress=lzo,ssd,noatime

# Create var $o_btrfs_sdd with default options for btrfs on SSD
o_btrfs_hdd=$o,compress=lzo

# Mount 'ROOT' subvolume
mount -t btrfs -o subvol=ROOT,$o_btrfs_ssd LABEL=system /mnt/ || { echo "Failed to mount 'ROOT' subvolume"; exit 7; }

# Mount 'var' subvolume
mount -t btrfs -o subvol=var,$o_btrfs_ssd LABEL=system /mnt/var || { echo "Failed to mount 'var' subvolume"; exit 7; }

# Mount 'tmp' subvolume
mount -t btrfs -o subvol=tmp,$o_btrfs_ssd LABEL=system /mnt/tmp || { echo "Failed to mount 'tmp' subvolume"; exit 7; }

if [ -z "$DUAL" ]
then
	# Mount 'home' subvolume
	mount -t btrfs -o subvol=home,$o_btrfs_ssd LABEL=system /mnt/home || { echo "Failed to mount 'home' subvolume"; exit 7; }
fi

echo "Done"

#####################################
## Mount home partition subvolumes ##
#####################################

if [ -z "$DUAL" ]
then
	printf "%s" "Mount home subvolumes ... "

	# Mount 'home' subvolume
	mount -t btrfs -o subvol=HOME,$o_btrfs_hdd LABEL=home /mnt/home || { echo "Failed to mount 'home' subvolume"; exit 8; }

	echo "Done"
fi
#############################################
## Mount EFI and boot partition subvolumes ##
#############################################

printf "%s" "Mount boot partition ... "

# Create boot mount point
mkdir -p /mnt/boot || { echo "Failed to create mount point for 'boot'"; exit 9; }

# Mount boot partition
mount LABEL=boot /mnt/boot || { echo "Failed to mount 'boot' partition"; exit 9; }

echo "Done"
printf "%s" "Mount efi partition ... "

# Create boot efi point
mkdir -p /mnt/boot/efi || { echo "Failed to create mount point for 'efi'"; exit 10; }

# Mount EFI partition
mount LABEL=EFI /mnt/boot/efi || { echo "Failed to mount 'efi' partition"; exit 10; }

echo "Done"
