#!/bin/bash

if [ "$#" -eq 4 ]
then
	HNAME=$1
	UNAME=$2
	UNAME_PASS=$3
	ROOT_PASS=$4
else
	echo "Arguments are not equals to 4: chroot_install.sh <HOST_NAME> <USER_NAME> <USER_PASS> <ROOT_PASS>"
    exit 1
fi

#####################
## Generate locale ##
#####################

# Select locale to generate
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen || { echo "Failed to set locale"; exit 1; }

# Generate locale
locale-gen
echo LANG=en_US.UTF-8 > /etc/locale.conf
export LANG=en_US.UTF-8

#########################
## Setup time and date ##
#########################

# Setup system clock
ln -sf /usr/share/zoneinfo/Europe/Prague /etc/localtime || { echo "Failed to set system clock"; exit 2; }
hwclock --systohc --utc || { echo "Failed to sync HW clock"; exit 2; }

##################
## Set hostname ##
##################

# Set the hostname
echo "$HNAME" > /etc/hostname

#################################
## Install additional packages ##
#################################

# Install rest of the system tools
pacman --noconfirm -S sudo  intel-ucode grub pkgfile efibootmgr intel-ucode base-devel btrfs-progs iw gptfdisk zsh terminus-font || { echo "Failed to install additional packages"; exit 3; }

pkgfile --update || { echo "Failed to update list of tools in pkgfile"; exit 3; }

##########################
## Create openswap hook ##
##########################

# Setup SWAP hook
cat > /etc/initcpio/hooks/openswap<<EOL
run_hook ()
{
    cryptsetup open /dev/disk/by-partlabel/cryptswap swap
}
EOL

# Install SWAP hook
cat > /etc/initcpio/install/openswap<<EOL
build ()
{
   add_runscript
}
help ()
{
cat<<HELPEOF
  This opens the swap encrypted partition /dev/disk/by-partlabel/cryptswap in /dev/mapper/swap
HELPEOF
}
EOL

# Replace hooks in /etc/mkinitcpio.conf
sed -i 's/^HOOKS.*/HOOKS=(base udev autodetect keyboard keymap consolefont modconf block encrypt btrfs filesystems fsck)/' /etc/mkinitcpio.conf

# Create the initramfs image
mkinitcpio -p linux || { echo "Failed to create initramfs"; exit 4; }

#cat >> /etc/fstab<<EOL
#/dev/mapper/swap swap swap defaults 0 0
#EOL

##################
## Install GRUB ##
##################

# Get the UUID of your root partition
UUID_SYSTEM=$(blkid -s UUID -o value /dev/disk/by-partlabel/cryptsystem)

# Install GRUB bootloader
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=grub_uefi --recheck || { echo "Failed to install GRUB"; exit 5; }
sed  -i 's#GRUB_CMDLINE_LINUX=""#GRUB_CMDLINE_LINUX="cryptdevice=UUID='"$UUID_SYSTEM"':system root=/dev/mapper/system resume=/dev/mapper/swap"#g' /etc/default/grub
grub-mkconfig -o /boot/grub/grub.cfg || { echo "Failed to configure GRUB"; exit 6; }

##################################
## User and password management ##
##################################

# Add user
groupadd "$UNAME" || { echo "Failed to create user group"; exit 7; }
useradd -m -g "$UNAME" -G wheel,storage,power,users -s /bin/bash "$UNAME" || { echo "Failed to create user"; exit 8; }

#TODO: Shred the passwords file for better protection
if [ -z "$QEMU" ]
then
    echo "root:$ROOT_PASS\n$UNAME:$UNAME_PASS" | chpasswd || { echo "Failed to change passwords"; exit 12; }
else
    # Set root password
    echo "Set root password:"
    while true; do
        passwd
        if passwd; then
            break
        else
            echo "Enter correct password"
        fi
    done

    echo "Set $USER password:"
    while true; do
        
        if passwd "$UNAME"; then
            break
        else
            echo "Enter correct password"
        fi
    done
fi

sed  -i 's#.*wheel ALL=(ALL) ALL#wheel ALL=(ALL) ALL#g' /etc/sudoers || { echo "Failed to grant powers to wheel group"; exit 9; }

############################
## Set IPTABLES v4 and v6 ##
############################

source /root/21_iptables_rules.sh || { echo "Failed to create firewall rules"; exit 10; }

#####################
## Run user script ##
#####################

source /root/25_kde_install.sh || { echo "Failed to install KDE5"; exit 11; }
