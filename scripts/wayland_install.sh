#################################
## Install Intel video drivers ##
#################################

pacman -S xf86-video-intel mesa-libgl libva-intel-driver libva

pacman -S ttf-dejavu ttf-droid fontconfig-ttf-ms-fonts

#########################
## Install audio stack ##
#########################

pacman -S alsa-utils pulseaudio pulseaudio-alsa

# Unmute all channels
amixer sset Master unmute

#############################
## Add AUR package manager ##
#############################

git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
	
# use this to see your interface
ip link
 
# Disable dhcpcd
systemctl stop dhcpcd@<interaface>.service
systemctl disable dhcpcd@<interaface>.service
systemctl stop dhcpcd.service
systemctl disable dhcpcd.service

systemctl start NetworkManager
systemctl enable NetworkManager


####################
## WIFI connection #
####################

wpa_passphrase <passphrase> | wpa_supplicant -B -i <SSID> -c /dev/stdin

####################
## Download mirror #
####################

nano /etc/pacman.d/mirrorlist

#################
## SYSTEMD-BOOT #
#################

# /boot/loader/entries/arch-encrypted.conf
title Arch Linux Encrypted
linux /vmlinuz-linux
initrd  /intel-ucode.img
initrd /initramfs-linux.img
options cryptdevice=UUID=<UUID>:<mapped-name> root=/dev/mapper/<mapped-name> quiet rw


To find the UUID of the drive use:
blkid -s UUID -o value /dev/mapper/root
	
# /boot/loader/entries/arch-encrypted-lvm.conf
title Arch Linux Encrypted LVM
linux /vmlinuz-linux
initrd  /intel-ucode.img
initrd /initramfs-linux.img
options cryptdevice=UUID=<UUID>:Vol root=/dev/mapper/Vol-root quiet rw


#######################
## Pacman cheat sheet #
#######################
	
# To remove a single package, leaving all of its dependencies installed
sudo pacman -R 
 
# To remove a package and its dependencies which are not required by any other installed package
sudo pacman -Rs 
 
# To remove a package, its dependencies and all the packages that depend on the target package:
# Warning: This operation is recursive, and must be used with care since it can remove many potentially needed packages.
sudo pacman -Rsc 
 
# To remove a package, which is required by another package, without removing the dependent package
sudo pacman -Rdd 
 
# pacman saves important configuration files when removing certain applications and names them with the extension: .pacsave. 
# To prevent the creation of these backup files use the -n option:
pacman -Rn 
 
# Force refresh of all package lists
pacman -Syy

###################
## Wayland hacks ##
###################

# Check if app uses wayland
ldd $application_path | grep wayland