#!/bin/bash

##############
## IPTABLES ##
##############

echo "Creating IPv4 firewall rules ..."
# Create chain TCP and UDP
iptables -N TCP
iptables -N UDP

# Disable forwarding
iptables -P FORWARD DROP

# Accept all outgoing traffic
iptables -P OUTPUT ACCEPT

# Accept established connections
iptables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT

# Drop all incoming traffic
iptables -P INPUT DROP

# Accept traffic on loopback interface
iptables -A INPUT -i lo -j ACCEPT

# Drop invalid traffic
iptables -A INPUT -m conntrack --ctstate INVALID -j DROP

# Drop ICMP Ping packets
iptables -A INPUT -p icmp --icmp-type 8 -m conntrack --ctstate NEW -j ACCEPT

# Forward UDP and TCP traffic in correct chains
iptables -A INPUT -p udp -m conntrack --ctstate NEW -j UDP
iptables -A INPUT -p tcp --syn -m conntrack --ctstate NEW -j TCP

# Drop packets if ports are not opened
iptables -A INPUT -p udp -j DROP
iptables -A INPUT -p tcp -j DROP

# Drop rest of the traffic
iptables -A INPUT -j DROP

# Allow SSH
iptables -A TCP -p tcp --dport 22 -j ACCEPT

# Allow KDE connect
iptables -A UDP -p udp --dport 1714:1764 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A TCP -p tcp --dport 1714:1764 -m state --state NEW,ESTABLISHED -j ACCEPT


echo "Storing IPv4 firewall rules to '/etc/iptables/iptables.rules'"
iptables-save > /etc/iptables/iptables.rules

echo "Enabling IP4 firewall"
systemctl enable iptables

###############
## IP6TABLES ##
###############

echo "Creating IPv6 firewall rules ..."
# Create chain TCP and UDP
ip6tables -N TCP
ip6tables -N UDP

# Disable forwarding
ip6tables -P FORWARD DROP

# Accept all outgoing traffic
ip6tables -P OUTPUT ACCEPT

# Accept established connections
ip6tables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT

# Drop all incoming traffic
ip6tables -P INPUT DROP

# Accept traffic on loopback interface
ip6tables -A INPUT -i lo -j ACCEPT

# Drop invalid traffic
ip6tables -A INPUT -m conntrack --ctstate INVALID -j DROP

# Drop ICMP Ping packets
ip6tables -A INPUT -p ipv6-icmp --icmpv6-type 128 -m conntrack --ctstate NEW -j ACCEPT

# Forward UDP and TCP traffic in correct chains
ip6tables -A INPUT -p udp -m conntrack --ctstate NEW -j UDP
ip6tables -A INPUT -p tcp --syn -m conntrack --ctstate NEW -j TCP

# Drop packets if ports are not opened
ip6tables -A INPUT -p udp -j DROP
ip6tables -A INPUT -p tcp -j DROP

# Drop rest of the traffic
ip6tables -A INPUT -j DROP

# Allow SSH
ip6tables -A TCP -p tcp --dport 22 -j ACCEPT

# Allow KDE connect
ip6tables -A UDP -p udp --dport 1714:1764 -m state --state NEW,ESTABLISHED -j ACCEPT
ip6tables -A TCP -p tcp --dport 1714:1764 -m state --state NEW,ESTABLISHED -j ACCEPT

echo "Storing IPv6 firewall rules to '/etc/iptables/ip6tables.rules'"
ip6tables-save > /etc/iptables/ip6tables.rules

echo "Enabling IP6 firewall"
systemctl enable ip6tables
