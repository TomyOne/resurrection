groupadd $UNAME
useradd -G $UNAME -mg users -G wheel,storage,power -s /bin/bash $UNAME

passwd $UNAME

pacman-key --init
pacman-key --populate archlinux

# Disable kernel module Intel ME (generates wakeup)
echo "blacklist mei" > /etc/modprobe.d/mei.conf

# TODO: install sudo package
# TODO: Add line %wheel ALL=(ALL) ALL in /etc/sudoers

# TODO: install pacman -S xorg-server-utils xorg-xinit

# TODO: Power management
# pacman -S powertop
# powertop --calibrate
# powertop --auto-tune

cat >>/etc/systemd/system/powertop.service<<EOL
[Unit]
Description=Powertop tunings

[Service]
Type=oneshot
ExecStart=/usr/bin/powertop --auto-tune

[Install]
WantedBy=multi-user.target
EOL

# systemctl enable powertop.service

# TODO: PAM secure login
$ grep pam_tally /etc/pam.d/system-login
#auth       required   pam_tally.so         onerr=succeed file=/var/log/faillog
auth       required   pam_tally.so deny=2 unlock_time=600 onerr=succeed file=/var/log/faillog

# TODO: Check SSHD settings
grep PermitRootLogin /etc/ssh/sshd_config
PermitRootLogin no

# TODO: IPTABLEs

#TODO: Dmesg access
$ cat /etc/sysctl.d/50-dmesg-restrict.conf

kernel.dmesg_restrict = 1