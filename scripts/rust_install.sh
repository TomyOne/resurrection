#!/bin/sh

RUST_TAR = rust-1.30.1-x86_64-unknown-linux-gnu.tar.gz
RUST_TAR_ASC = $RUST_TAR.asc
RUST_KEY = rust-key.gpg.ascii

URL_RUST_TAR = https://static.rust-lang.org/dist/$RUST_TAR
URL_RUST_TAR_ASC = https://static.rust-lang.org/dist/$RUST_TAR_ASC
URL_RUST_KEY = https://static.rust-lang.org/$RUST_KEY


# Download the files
curl $URL_RUST_TAR --output $RUST_TAR
curl $URL_RUST_TAR_ASC --output $RUST_TAR_ASC
curl $URL_RUST_KEY --output $RUST_KEY

#Import public key
gpg --import $RUST_KEY

# Verify the Rust installer
gpg --verify $RUST_TAR_ASC $RUST_TAR
