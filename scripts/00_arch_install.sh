#!/bin/sh

echo "Installing Archlinux to the system ..."

if [ -z "$HNAME" ]; then echo "Please set variable HNAME for installation"; exit 1; else echo "HNAME is set to '$HNAME'"; fi
if [ -z "$UNAME" ]; then echo "Please set variable UNAME for installation"; exit 1; else echo "UNAME is set to '$UNAME'"; fi

if [ -z "$QEMU" ]
then
    if [ -z "$UNAME_PASS" ]; then echo "Please set variable UNAME_PASS for installation"; exit 1; else echo "UNAME_PASS is set to '$UNAME_PASS'"; fi
    if [ -z "$ROOT_PASS" ]; then echo "Please set variable ROOT_PASS for installation"; exit 1; else echo "ROOT_PASS is set to '$ROOT_PASS'"; fi
    echo "Running in QEMU ..."
    if [ -z "$SYSTEM_PASS" ]; then export SYSTEM_PASS=system; echo "Setting SYSTEM_PASS to '$SYSTEM_PASS'"; else echo "SYSTEM_PASS set by user"; fi
    if [ -z "$HOME_PASS" ]; then export HOME_PASS=home; echo "Setting HOME_PASS to '$HOME_PASS'";  else echo "HOME_PASS set by user"; fi
    if [ -z "$SWAP_PASS" ]; then export SWAP_PASS=swap; echo "Setting SWAP_PASS to '$SWAP_PASS'"; else echo "SWAP_PASS set by user"; fi
fi

if [ -z "$SSD" ]; then echo "Please set variable SSD to the disk for installation"; exit 1; else echo "SSD is set to '$SSD'"; fi
if [ -z "$DUAL" ]; then if [ -z "$HDD" ]; then echo "Please set variable HDD to the disk for installation"; exit 1; else echo "HDD is set to '$HDD'"; fi ;fi

. ./10_partition.sh || { echo "Failed to partition the disk/s" && exit 2; }
. ./11_mount.sh || { echo "Failed to mount partitions" && exit 3; }
. ./12_base_install.sh || { echo "Failed to install the system" && exit 4; }
