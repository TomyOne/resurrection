#!/bin/bash

##########################
## Begin system install ##
##########################

printf "%s" "Install base system ... "

# Install base packages to /mnt
pacstrap /mnt base || { echo "Failed to install base system"; exit 1; }

echo "Done"

##################################################
## Copy the home and swap keys, modify crypttab ##
##################################################

if [ -z "$DUAL" ]
then
	printf "%s" "Copy key files for home, swap and modify crypttab ... "

	## Copy keyfiles
	cp /home.key /swap.key /mnt/root || { echo "Failed to copy key files for 'home' and 'swap' partitions"; exit 2; }

	UUID_CRYPTHOME=$(blkid -s UUID -o value /dev/disk/by-partlabel/crypthome)
	UUID_CRYPTSWAP=$(blkid -s UUID -o value /dev/disk/by-partlabel/cryptswap)
	# Add entries to crypttab
cat >> /mnt/etc/crypttab <<EOL
home    UUID=$UUID_CRYPTHOME    /root/home.key
swap    UUID=$UUID_CRYPTSWAP    /root/swap.key
EOL
else
	printf "Copy key files for swap and modify crypttab ... "

	## Copy keyfiles
	cp /swap.key /mnt/root || { echo "Failed to copy key file for 'swap' partition"; exit 2; }

	UUID_CRYPTSWAP=$(blkid -s UUID -o value /dev/disk/by-partlabel/cryptswap)
	# Add entries to crypttab
cat >> /mnt/etc/crypttab <<EOL
swap    UUID=$UUID_CRYPTSWAP    /root/swap.key
EOL
fi

echo "Done"

####################
## Generate fstab ##
####################

printf "%s" "Generate fstab ... "

# TODO: TMPFS
# tmpfs	/tmp	tmpfs	defaults,noatime,mode=1777	0	0

# Generate filesystem table
genfstab -L -p /mnt >> /mnt/etc/fstab || { echo "Failed to generate fstab"; exit 3; }

if [ -z "$DUAL" ]
then
sed -i 's#LABEL="home"#/dev/mapper/home#g' /mnt/etc/fstab || { echo "Failed to replace home by partlabel"; exit 4; }
fi
sed -i 's#LABEL="swap"#/dev/mapper/swap#g' /mnt/etc/fstab || { echo "Failed to replace swap by partlabel"; exit 4; }

echo "Done"

##################
## Enter chroot ##
##################

printf "%s" "Enter chroot ... "

cp 2*.sh /mnt/root/ || { echo "Failed to copy chroot installation script"; exit 5; }

# chroot into the new system
arch-chroot /mnt /root/20_chroot_install.sh "$HNAME" "$UNAME" "$UNAME_PASS" "$ROOT_PASS" || { echo "Failed to run chroot installation script"; exit 5; }

echo "Done"

####################
## Finish install ##
####################

printf "%s" "Finish install ... "

# Readout the OEM key
OEM_KEY="$(grep -aPo '[\w]{5}-[\w]{5}-[\w]{5}-[\w]{5}-[\w]{5}' /sys/firmware/acpi/tables/MSDM)"

echo "Windows key: $OEM_KEY"
echo "$OEM_KEY" > /mnt/root/oem_key.txt

echo "Done. Goodbye !!!"

sleep 3
poweroff
