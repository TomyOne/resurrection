#!/bin/bash

if [ -z "$SSD" ]; then echo "Please set variable SSD to the disk for installation"; exit 1; else echo "SSD is set to '$SSD'"; fi
if [ -z "$DUAL" ]; then if [ -z "$HDD" ]; then echo "Please set variable HDD to the disk for installation"; exit 1; else echo "HDD is set to '$HDD'"; fi ;fi

#######################
## Create partitions ##
#######################

printf "%s" "Wiping disk $SDD ... "
# Remove legacy partition infromation
sgdisk --zap-all "$SSD" || { echo "Failed to wipe SSD"; exit 2; }
echo "Done"

if [ -z "$DUAL" ]
then
  printf "%s" "Wiping disk $HDD ... "
  sgdisk --zap-all "$HDD" || { echo "Failed to wipe HDD"; exit 2; }
  echo "Done"
fi

echo "Done"
printf "%s" "Creating new partitions ... "

# Create three partitions: EFI, encrypted swap and system
if [ -z "$DUAL" ]
then
  echo "Dual drives"
  sgdisk --clear \
         --new=1:0:+256MiB --typecode=1:ef00 --change-name=1:EFI \
         --new=2:0:+512MiB --typecode=2:8200 --change-name=2:boot \
         --new=3:0:0       --typecode=3:8200 --change-name=3:cryptsystem \
           "$SSD" || { echo "Failed to create partitions on SSD"; exit 3; }

  sgdisk --clear \
         --new=1:0:+8GiB   --typecode=1:8200 --change-name=1:cryptswap \
         --new=2:0:0       --typecode=2:8200 --change-name=2:crypthome \
           "$HDD" || { echo "Failed to create partitions on HDD"; exit 3; }
else
  echo "Single drive"
  sgdisk --clear \
         --new=1:0:+256MiB --typecode=1:ef00 --change-name=1:EFI \
         --new=2:0:+512MiB --typecode=2:8200 --change-name=2:boot \
         --new=3:0:+8GiB   --typecode=3:8200 --change-name=3:cryptswap \
         --new=4:0:0       --typecode=4:8200 --change-name=4:cryptsystem \
           "$SSD" || { echo "Failed to create partitions on SSD"; exit 3; }
fi
echo "Done"

######################
## Check partitions ##
######################

printf "%s" "Check new partitions exist ... "

# Wait for the changes to sync to disks
sleep 10

if [ ! -L "/dev/disk/by-partlabel/EFI" ]
then
	echo "EFI partition not found"; exit 3
fi

if [ ! -L "/dev/disk/by-partlabel/boot" ]
then
	echo "boot partition not found"; exit 3
fi

if [ ! -L "/dev/disk/by-partlabel/cryptswap" ]
then
	echo "cryptswap partition not found"; exit 3
fi

if [ ! -L "/dev/disk/by-partlabel/cryptsystem" ]
then
	echo "cryptsystem partition not found"; exit 3
fi

if [ -z "$DUAL" ]
then
  if [ ! -L "/dev/disk/by-partlabel/crypthome" ]
  then
  	echo "crypthome partition not found"; exit 3
  fi
fi

echo "Done"

####################################
## Create EFI and boot partitions ##
####################################

printf "%s" "Format EFI partition ... "

# Create FAT32 filesystem for EFI partition
mkfs.fat -F32 -n EFI /dev/disk/by-partlabel/EFI || { echo "Failed to create 'EFI' filesystem"; exit 4; }

echo "Done"
printf "%s" "Format boot partition ... "

# Create ext2 boot partition
mkfs.ext2 -L boot /dev/disk/by-partlabel/boot || { echo "Failed to create 'boot' filesystem"; exit 5; }

echo "Done"

#######################################
## Create encrypted system partition ##
#######################################

printf "%s" "Create encrypted system partition ... "

if [ -z "$QEMU" ]
then
    echo "QEMU branch"
    
    # Encrypt system partition
    echo -n "$SYSTEM_PASS" |cryptsetup luksFormat --align-payload=8192 -s 256 -c aes-xts-plain64 /dev/disk/by-partlabel/cryptsystem -d - || { echo "Failed to create encrypted 'system' partition"; exit 6; }
    
    # Open system partition
    echo -n "$SYSTEM_PASS" | cryptsetup open /dev/disk/by-partlabel/cryptsystem system -d - || { echo "Failed to decrypt and mount encrypted 'system' partition"; exit 7; }
else
    # Encrypt system partition
    while true; do
        cryptsetup luksFormat --align-payload=8192 -s 256 -c aes-xts-plain64 /dev/disk/by-partlabel/cryptsystem
        RET=$?
        
        if [ $RET -eq 0 ]; then
            break
        elif [ $RET -eq 1 ]; then
            echo "!!! Type YES !!!"
        elif [ $RET -eq 2 ]; then
            echo "Enter correct password"
        else
            echo "Failed to create encrypted 'system' partition"; exit 6
        fi
    done
        
    # Open system partition
    while true; do
        cryptsetup open /dev/disk/by-partlabel/cryptsystem system
        RET=$?
        
        if [ $RET -eq 0 ]; then
            break
        elif [ $RET -eq 1 ]; then
            echo "Enter correct password"
        else
            echo "Failed to decrypt and mount encrypted 'system' partition"; exit 7
        fi
    done
fi

# Create Btrfs filesystem on system
mkfs.btrfs --force --label system /dev/mapper/system || { echo "Failed to create 'system' btrfs partition"; exit 8; }

echo "Done"

#####################################
## Create encrypted home partition ##
#####################################

if [ -z "$DUAL" ]
then
    printf "%s" "Create encrypted home partition ... "

    if [ -z "$QEMU" ]
    then
        echo "QEMU branch"
        
        # Encrypt home partition
        echo -n "$HOME_PASS" | cryptsetup luksFormat --align-payload=8192 -s 256 -c aes-xts-plain64 /dev/disk/by-partlabel/crypthome -d - || { echo "Failed to create encrypted 'home' partition"; exit 9; }

        # Open system partition
        echo -n "$HOME_PASS" | cryptsetup open /dev/disk/by-partlabel/crypthome home -d - || { echo "Failed to decrypt and mount encrypted 'home' partition"; exit 11; }
    else
        # Encrypt home partition
        while true; do
            cryptsetup luksFormat --align-payload=8192 -s 256 -c aes-xts-plain64 /dev/disk/by-partlabel/crypthome
            RET=$?
            
            if [ $RET -eq 0 ]; then
                break
            elif [ $RET -eq 1 ]; then
                echo "!!! Type YES !!!"
            elif [ $RET -eq 2 ]; then
                echo "Enter correct password"
            else
                echo "Failed to create encrypted 'home' partition"; exit 9
            fi
        done

        # Open system partition
        while true; do
            cryptsetup open /dev/disk/by-partlabel/crypthome home
            RET=$?
            
            if [ $RET -eq 0 ]; then
                break
            elif [ $RET -eq 1 ]; then
                echo "Enter correct password"
            else
                echo "Failed to decrypt and mount encrypted 'home' partition"; exit 11
            fi
        done
    fi

    ## Create keyfile of 2048 random byte
    dd bs=512 count=4 if=/dev/urandom of=/home.key iflag=fullblock || { echo "Failed to create keyfile for 'home'"; exit 10; }

    # Create Btrfs filesystem on home
    mkfs.btrfs --force --label home /dev/mapper/home || { echo "Failed to create 'home' btrfs partition"; exit 12; }

    echo "Done"
fi

#####################################
## Create encrypted swap partition ##
#####################################

printf "%s" "Create encrypted swap partition ... "

if [ -z "$QEMU" ]
then
    echo "QEMU branch"
    # Encrypt swap partition
    echo -n "$SWAP_PASS" | cryptsetup luksFormat /dev/disk/by-partlabel/cryptswap -d - || { echo "Failed to create encrypted 'swap' partition"; exit 13; }
    
    #Open swap partition
    echo -n "$SWAP_PASS" | cryptsetup open /dev/disk/by-partlabel/cryptswap swap -d - || { echo "Failed to decrypt and mount encrypted 'swap' partition"; exit 15; }
    
else
    # Encrypt swap partition
    while true; do
        cryptsetup luksFormat /dev/disk/by-partlabel/cryptswap
        RET=$?
        
        if [ $RET -eq 0 ]; then
            break
        elif [ $RET -eq 1 ]; then
            echo "!!! Type YES !!!"
        elif [ $RET -eq 2 ]; then
            echo "Enter correct password"
        else
            echo "Failed to create encrypted 'swap' partition"; exit 13
        fi
    done

    #Open swap partition
    while true; do
        cryptsetup open /dev/disk/by-partlabel/cryptswap swap
        RET=$?
        
        if [ $RET -eq 0 ]; then
            break
        elif [ $RET -eq 1 ]; then
            echo "Enter correct password"
        else
            echo "Failed to decrypt and mount encrypted 'swap' partition"; exit 15
        fi
    done
fi

## Create keyfile of 2048 random bytes
dd bs=512 count=4 if=/dev/urandom of=/swap.key iflag=fullblock || { echo "Failed to create keyfile for 'swap'"; exit 14; }

# Create swap partition
mkswap -L swap /dev/mapper/swap || { echo "Failed to create 'swap' partition"; exit 16; }

# Mount swap
swapon -L swap || { echo "Failed to mount swap"; exit 17; }

echo "Done"

###################################################
## Create key files for home and swap partitions ##
###################################################

if [ -z "$DUAL" ]
then
  printf "%s" "Add keyfiles for 'home' and 'swap' partitions ... "
  if [ -z "$QEMU" ]
  then
    ## Add keyfile
    echo -n "$HOME_PASS" | cryptsetup luksAddKey /dev/disk/by-partlabel/crypthome /home.key -d - || { echo "Failed to add key file for 'home' partition"; exit 18; }
  else
    ## Add keyfile
    while true; do
        cryptsetup luksAddKey /dev/disk/by-partlabel/crypthome /home.key
        RET=$?
        
        if [ $RET -eq 0 ]; then
            break
        elif [ $RET -eq 1 ]; then
            echo "Enter correct password"
        else
            echo "Failed to add key file for 'home' partition"; exit 18
        fi
    done
  fi
else
  printf "%s" "Add keyfiles for 'swap' partition ... "
fi

if [ -z "$QEMU" ]
then
    echo -n "$SWAP_PASS" | cryptsetup luksAddKey /dev/disk/by-partlabel/cryptswap /swap.key -d - || { echo "Failed to ad key file for 'swap' partition"; exit 18; }
else
    ## Add keyfile
    while true; do
        cryptsetup luksAddKey /dev/disk/by-partlabel/cryptswap /swap.key
        RET=$?
        
        if [ $RET -eq 0 ]; then
            break
        elif [ $RET -eq 2 ]; then
            echo "Enter correct password"
        else
            echo "Failed to ad key file for 'swap' partition"; exit 18
        fi
    done
fi

echo "Done"
