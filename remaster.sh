#!/bin/sh

# Check for path to ISO
if [ -z "$ARCH_ISO" ]
then
      echo "Please set var ARCH_ISO to path pointing to ISO image!"
      exit 1
fi

# Check if ISO exists
if [! -f "$ARCH_ISO" ]
then
      echo "ARCH_ISO path is not valid!"
      exit 1
fi

# Create temporary dir to mount the ISO
mkdir archiso || (echo "Failed to create mount dir for ISO" && exit 1)

# Mount the ISO
sudo mount -t iso9660 -o loop $ARCH_ISO archiso || (echo "Failed to mount the ISO" && exit 2)

# Copy the data as ISO is read-only
cp -a archiso/* rustarch || (echo "Failed to copy files" && exit 3)

# Change directory
cd rustarch/arch/x86_64 || (echo "Failed to cd into dir" && exit 4)

# Unsquash the root
unsquashfs airootfs.sfs || (echo "Failed to unsquash the root" && exit 5)

# Copy temporarily kernel
cp ../boot/x86_64/vmlinuz squashfs-root/boot/vmlinuz-linux || (echo "Failed to copy kernel" && exit 6)

# Chroot to unsquashed root and run script
arch-chroot squashfs-root remaster_chroot.sh || (echo "Failed to execute script in chroot" && exit 7)

# Move package list
mv squashfs-root/pkglist.txt archrust/arch/pkglist.x86_64.txt || (echo "Failed to move package list" && exit 8)

# Remove old squash root
rm airootfs.sfs || (echo "Failed to remove old squash root" && exit 9)

# Create new squash root
mksquashfs squashfs-root airootfs.sfs || (echo "Failed to create new squash root" && exit 10)

# Remove new unsquash root
rm -r squashfs-root || (echo "Failed to remove new unsquash root" && exit 11)

# Generate MD5 sum
md5sum airootfs.sfs > airootfs.md5 || (echo "Failed to generate MD5 sum" && exit 12)

# Create new ISO
iso_label="ARCH_201209"
xorriso -as mkisofs \
       -iso-level 3 \  
       -full-iso9660-filenames \
       -volid "${iso_label}" \
       -eltorito-boot isolinux/isolinux.bin \
       -eltorito-catalog isolinux/boot.cat \
       -no-emul-boot -boot-load-size 4 -boot-info-table \
       -isohybrid-mbr ~/customiso/isolinux/isohdpfx.bin \
       -output archrust.iso \ 
       rustarch || (echo "Failed to create new ISO" && exit 13)
       
# Unmount original ISO
sudo umount archiso || (echo "Failed to unmount original ISO" && exit 14)

# Remove mount point
rmdir archiso || (echo "Failed to remove mount point" && exit 15)

